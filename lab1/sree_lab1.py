#print("hello world")

def decimal_to_binary(num):
    #logic to convert decimal to binary
    #divide the number till it reaches 1
    #keep saving the quotient and remainder for next operation
    #when the quotient reaches 1 we have our binary number

    binary_num = ''
    if (num==0):
        return "0b0"
    if (num < 0):
        return "Please provide positive numbers"
    
    while (num >= 1):
        quotient = num // 2 #integer division
        remainder = num % 2 # get remainder
        binary_num = str(remainder) + binary_num

        num = quotient
    
    return '0b' + binary_num

result = decimal_to_binary(1) # 1000
print(result)